# Test MC Requests

A template repository for testing MC requests using the [MCProdTester](https://gitlab.cern.ch/kkrizka/mcprodtester/) project.

It can be used in two ways:
- Making a fork and setting up the grid certificate for private tests.
- Requesting Developer access to this repository and using a separate branch.

Instructions for creating a request YAML definition are in the MCProdTester README.

## Forking Repository
If you fork the repository, you need to setup the following CI variables using your grid certificate:
- `GRID_CERT`: Base64 encoded value of your grid certificate (`base64 -i ~/.globus/usercern.pem`).
- `GRID_KEY`: Base64 encoded value of your grid key (`base64 -i ~/.globus/userkey.pem`).
- `GRID_PASS`: Your certificate password surrounded by `=`. (ie: `=password=`).

## Custom Branch (Recommended)
The recommended way to prepare a request test is to commit directly to a branch of this repository. This has the following avantages:
- No need to setup the grid certificate. All is ready to go!
- Central place to monitor all requests.

The use this repostory:
1. Email to request `Developer` role from the repository owner (most likely kkrizka@cern.ch).
2. Clone the repository.
```shell
git clone --recursive ssh://git@gitlab.cern.ch:7999/kkrizka/mcrequest.git
```
3. Enter the repository and install the MCProdTester project in a virtual environment. The following instructions are for a CentOS7 system with CVMFS (ie: lxplus).
```shell
cd mcrequest
lsetup 'lcgenv -p LCG_101_ATLAS_14 x86_64-centos7-gcc11-opt Python'
python -m venv venv
cat mcprodtester/postactivate >> venv/bin/activate
source venv/bin/activate
pip install mcprodtester
install -D /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/rucio-clients/1.27.9/etc/rucio.cfg ${VIRTUAL_ENV}/etc/rucio.cfg
install -D /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/rucio-clients/1.27.9/etc/ca.crt ${VIRTUAL_ENV}/etc/ca.crt
```
4. Enter the repository and create your custom branch. Pushes to `main` are protected.
```shell
git checkout -b ${USER}_requestname
```
5. Define the request in the provided `request.yaml` file and run local tests. Instructions are available in the [MCProdTester](https://gitlab.cern.ch/kkrizka/mcprodtester/) README.
6. Commit your changes and push the branch to GitLab.
7. Create a MR to `main` with your changes. The MR can be used to discuss with experts any errors in the failing CI pipeline.
8. Continue updating the request with changes as necessary. For example, update to offical tags once any custom changes are registered to AMI.
9. One the request is submitted, close the MR.
